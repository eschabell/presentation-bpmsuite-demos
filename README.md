Red Hat JBoss BPM Suite Demo Projects
-------------------------------------
View presentation here: [https://eschabell.gitlab.io/presentation-bpmsuite-demos](https://eschabell.gitlab.io/presentation-bpmsuite-demos)


![Cover Slide](https://raw.githubusercontent.com/eschabell/presentation-bpmsuite-demos/master/cover.png)


Released versions
-----------------

- v1.0 - session delivered on Dec 17, 2015.
